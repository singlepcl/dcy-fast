package com.dcy.modules.system.controller;

import com.dcy.common.annotation.Log;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.BaseController;
import com.dcy.modules.system.dto.input.ResourcesCreateInputDTO;
import com.dcy.modules.system.dto.input.ResourcesUpdateInputDTO;
import com.dcy.modules.system.dto.output.ResourcesListOutputDTO;
import com.dcy.modules.system.dtomapper.MResourcesMapper;
import com.dcy.system.model.Resources;
import com.dcy.system.service.ResourcesService;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/8/26 9:31
 */
@RestController
@RequestMapping("/system/resources")
@ApiSupport(order = 10)
@Api(value = "ResourcesController", tags = {"资源接口"})
public class ResourcesController extends BaseController<ResourcesService, Resources> {

    @Autowired
    private MResourcesMapper mResourcesMapper;

    @Log
    @ApiOperation(value = "获取tree-table列表数据", notes = "获取tree-table列表数据")
    @GetMapping(value = "/getResourceTreeTableList")
    public R<List<ResourcesListOutputDTO>> getModuleTreeTableList() {
        List<Resources> resourceTreeTableList = baseService.getResourceTreeTableList();
        List<ResourcesListOutputDTO> resourcesListOutputDTOS = mResourcesMapper.resourcesToResourcesListOutputDTO(resourceTreeTableList);
        return success(resourcesListOutputDTOS);
    }

    @Log
    @ApiOperation(value = "获取tree列表数据", notes = "获取tree列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色id", dataType = "roleId", paramType = "query")
    })
    @GetMapping(value = "/getResourceTreeListByRoleId")
    public R<List<String>> getResourceTreeListByRoleId(@NotBlank(message = "角色id不能为空") @RequestParam String roleId) {
        return success(baseService.getResourceTreeListByRoleId(roleId));
    }

    @Log
    @ApiOperation(value = "添加", notes = "添加")
    @PostMapping(value = "/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody ResourcesCreateInputDTO resourcesCreateInputDTO) {
        Resources resources = mResourcesMapper.resourcesCreateInputDTOToResources(resourcesCreateInputDTO);
        return success(baseService.saveOrUpdate(resources));
    }

    @Log
    @ApiOperation(value = "修改", notes = "修改")
    @PostMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody ResourcesUpdateInputDTO updateResourcesInputDTO) {
        Resources resources = mResourcesMapper.resourcesUpdateInputDTOToResources(updateResourcesInputDTO);
        return success(baseService.saveOrUpdate(resources));
    }

    @Log
    @ApiOperation(value = "删除", notes = "删除")
    @ApiImplicitParam(name = "id", value = "资源id", dataType = "String", paramType = "query", required = true)
    @PostMapping(value = "/delete")
    public R<Boolean> delete(@Validated @NotBlank(message = "资源id不能为空") @RequestParam String id) {
        return success(baseService.removeById(id));
    }
}
