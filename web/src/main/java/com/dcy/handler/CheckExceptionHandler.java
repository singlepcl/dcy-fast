package com.dcy.handler;

import com.dcy.common.model.R;
import com.dcy.common.model.ReturnCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.util.Set;

/**
 * @Author：dcy
 * @Description: 校验参数异常处理
 * @Date: 2021/6/2 8:38
 */
@RestControllerAdvice
@Order(-10)
@Slf4j
public class CheckExceptionHandler {

    /**
     * 校验异常
     *
     * @param bindException
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(BindException.class)
    public R<String> bindExceptionExceptionHandler(BindException bindException) {
        log.warn("bindException [{}]", bindException.toString());
//        validException.printStackTrace();
        if (bindException.hasErrors()) {
            for (ObjectError error : bindException.getAllErrors()) {
                return R.error(ReturnCode.CHECK_ERROR.getCode(), error.getDefaultMessage());
            }
        }
        return R.error(ReturnCode.CHECK_ERROR.getCode(), bindException.getMessage());
    }

    /**
     * 参数未填写 @RequestParam
     *
     * @param missingServletRequestParameterException
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public R<String> missingServletRequestParameterExceptionHandler(MissingServletRequestParameterException missingServletRequestParameterException) {
        log.warn("missingServletRequestParameterException [{}]", missingServletRequestParameterException.toString());
        return R.error(ReturnCode.CHECK_ERROR.getCode(), missingServletRequestParameterException.getMessage());
    }

    /**
     * 基本数据类型，验证错误
     *
     * @param exception
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(ValidationException.class)
    public R<String> validationExceptionHandler(ValidationException exception) {
        log.warn("validationExceptionHandle [{}]", exception.toString());
//        exception.printStackTrace();
        if (exception instanceof ConstraintViolationException) {
            ConstraintViolationException exs = (ConstraintViolationException) exception;
            Set<ConstraintViolation<?>> violations = exs.getConstraintViolations();
            for (ConstraintViolation<?> item : violations) {
                return R.error(ReturnCode.CHECK_ERROR.getCode(), item.getMessage());
            }
        }
        return R.error(ReturnCode.CHECK_ERROR.getCode(), exception.getMessage());
    }

}
