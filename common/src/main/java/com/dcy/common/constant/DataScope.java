package com.dcy.common.constant;

import cn.hutool.core.util.StrUtil;

public enum DataScope {

    // 数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）
    ALL("1", "全部数据权限"),
    CUSTOM("2", "自定数据权限"),
    ME_DEPT("3", "本部门数据权限"),
    ME_DEPT_CHILD("4", "本部门及以下数据权限");

    private String code;
    private String name;

    DataScope(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static DataScope getDataScopeByCode(String code) {
        if (StrUtil.isBlank(code)) {
            return null;
        }
        for (DataScope enums : DataScope.values()) {
            if (enums.getCode().equals(code)) {
                return enums;
            }
        }
        return null;
    }
}
