package com.dcy.workflow.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/6/7 11:12
 */
@Getter
@Setter
@ApiModel(value = "ModelCreateInputDTO", description = "创建模型表单")
public class ModelCreateInputDTO {

    @ApiModelProperty(value = "业务key")
    private String key;

    @ApiModelProperty(value = "类别")
    private String category;

    @ApiModelProperty(value = "流程模型名称")
    private String name;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "流程文件")
    private String jsonXml;

    @ApiModelProperty(value = "svg图片")
    private String svgXml;
}
