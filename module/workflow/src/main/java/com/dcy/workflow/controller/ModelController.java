package com.dcy.workflow.controller;

import cn.hutool.core.util.PageUtil;
import cn.hutool.core.util.StrUtil;
import com.dcy.common.model.R;
import com.dcy.db.base.model.PageResult;
import com.dcy.workflow.dto.input.ModelCreateInputDTO;
import com.dcy.workflow.dto.input.ModelSearchInputDTO;
import com.dcy.workflow.dto.input.ModelUpdateInputDTO;
import com.dcy.workflow.dto.output.ModelListOutputDTO;
import com.dcy.workflow.service.ActModelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.Model;
import org.flowable.engine.repository.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/6/7 10:55
 */
@Slf4j
@RestController
@RequestMapping("/flow/model")
@Api(value = "ModelController", tags = {"模型操作接口"})
public class ModelController {

    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private ActModelService actModelService;

    @ApiOperation(value = "获取模型列表")
    @GetMapping(value = "/page")
    public R<PageResult<ModelListOutputDTO>> page(ModelSearchInputDTO modelSearchInputDTO) {
        PageResult<ModelListOutputDTO> pageResult = new PageResult<>();
        ModelQuery modelQuery = repositoryService.createModelQuery().latestVersion().orderByLastUpdateTime().desc();
        if (StrUtil.isNotBlank(modelSearchInputDTO.getCategory())) {
            modelQuery.modelCategoryLike(StrUtil.format("%{}%", modelSearchInputDTO.getCategory()));
        }
        if (StrUtil.isNotBlank(modelSearchInputDTO.getName())) {
            modelQuery.modelNameLike(StrUtil.format("%{}%", modelSearchInputDTO.getName()));
        }
        if (StrUtil.isNotBlank(modelSearchInputDTO.getKey())) {
            modelQuery.modelKey(modelSearchInputDTO.getKey());
        }
        long count = modelQuery.count();
        PageUtil.setFirstPageNo(1);
        pageResult.setCurrent(modelSearchInputDTO.getCurrent());
        pageResult.setPages(PageUtil.totalPage((int) count, (int) modelSearchInputDTO.getSize()));
        List<ModelListOutputDTO> list = new ArrayList<>();
        List<Model> modelList = modelQuery.listPage(PageUtil.getStart((int) modelSearchInputDTO.getCurrent(), (int) modelSearchInputDTO.getSize()), (int) modelSearchInputDTO.getSize());
        modelList.forEach(model -> {
            list.add(new ModelListOutputDTO(model));
        });
        pageResult.setRecords(list);
        pageResult.setSize(modelSearchInputDTO.getSize());
        pageResult.setTotal(count);
        return R.success(pageResult);
    }

    @ApiOperation(value = "创建流程")
    @PostMapping(value = "/create")
    public R<Boolean> createModel(@RequestBody ModelCreateInputDTO modelSaveInputDTO) {
        boolean success = actModelService.createModel(modelSaveInputDTO.getKey(),
                modelSaveInputDTO.getName(),
                modelSaveInputDTO.getCategory(),
                modelSaveInputDTO.getDescription(),
                modelSaveInputDTO.getJsonXml(),
                modelSaveInputDTO.getSvgXml());
        return R.success(success);
    }

    @ApiOperation(value = "修改流程")
    @PostMapping(value = "/update")
    public R<Boolean> updateModel(@RequestBody ModelUpdateInputDTO modelUpdateInputDTO) {
        boolean success = actModelService.updateModel(modelUpdateInputDTO.getKey(),
                modelUpdateInputDTO.getId(),
                modelUpdateInputDTO.getName(),
                modelUpdateInputDTO.getCategory(),
                modelUpdateInputDTO.getDescription(),
                modelUpdateInputDTO.getJsonXml(),
                modelUpdateInputDTO.getSvgXml());
        return R.success(success);
    }

    @ApiOperation(value = "获取流程json信息")
    @ApiImplicitParam(name = "modelId", value = "模型ID", dataType = "String", paramType = "query", required = true)
    @GetMapping(value = "/getModelJsonById")
    public R<String> getModelJsonById(String modelId) {
        return R.success(actModelService.getModelJsonByModelId(modelId));
    }


    @ApiOperation(value = "部署模型")
    @ApiImplicitParam(name = "modelId", value = "模型id", dataType = "String", paramType = "query")
    @PostMapping(value = "deploy")
    public R<Boolean> deploy(String modelId) {
        return R.success(actModelService.deployModel(modelId));
    }


    @ApiOperation(value = "删除模型")
    @ApiImplicitParam(name = "modelId", value = "模型id", dataType = "String", paramType = "query")
    @PostMapping(value = "delete")
    public R<Boolean> delete(String modelId) {
        repositoryService.deleteModel(modelId);
        return R.success(true);
    }

}
