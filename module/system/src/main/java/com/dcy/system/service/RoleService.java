package com.dcy.system.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.enums.SqlMethod;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.dcy.common.constant.Constant;
import com.dcy.common.constant.DataScope;
import com.dcy.db.base.service.BaseService;
import com.dcy.system.mapper.*;
import com.dcy.system.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2020-08-19
 */
@Service
public class RoleService extends BaseService<RoleMapper, Role> {

    @Autowired
    private RoleResMapper roleResMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private RoleDeptMapper roleDeptMapper;
    @Autowired
    private DeptMapper deptMapper;

    /**
     * 根据角色id查询已授权的权限列表
     *
     * @param roleId
     * @return
     */
    public List<Resources> getAuthResourceListByRoleId(String roleId) {
        return baseMapper.getAuthResourceListByRoleId(roleId);
    }

    /**
     * 保存授权权限
     *
     * @param roleId 角色Id
     * @param resIds 授权权限Ids
     * @return
     */
    @CacheEvict(value = {"user:role", "user:resource"}, allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveAuthResource(String roleId, List<String> resIds) {
        boolean success = false;
        if (StrUtil.isNotBlank(roleId) && resIds != null) {
            // 删除关联表
            roleResMapper.delete(new LambdaQueryWrapper<RoleRes>().eq(RoleRes::getRoleId, roleId));
            // 添加关联表
            List<RoleRes> roleRes = new ArrayList<>();
            resIds.forEach(resId -> roleRes.add(new RoleRes().setRoleId(roleId).setResId(resId)));
            SqlHelper.executeBatch(RoleRes.class, this.log, roleRes, DEFAULT_BATCH_SIZE, (sqlSession, entity) -> {
                String sqlStatement = SqlHelper.getSqlStatement(RoleResMapper.class, SqlMethod.INSERT_ONE);
                sqlSession.insert(sqlStatement, entity);
            });
            success = true;
        }
        return success;
    }

    /**
     * 根据角色对象分页查询
     *
     * @param role
     * @return
     */
    public IPage<Role> getPageListByEntity(Role role) {
        LambdaQueryWrapper<Role> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(role.getRoleName()), Role::getRoleName, role.getRoleName());
        return super.page(role, queryWrapper);
    }

    /**
     * 保存角色数据以及自定义权限
     *
     * @param role
     * @param deptIds
     * @return
     */
    @CacheEvict(value = {"role:data-scope", "role:data-scope-flag"}, allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public boolean saveRole(Role role, List<String> deptIds) {
        boolean success = saveOrUpdate(role);
        // 删除数据
        roleDeptMapper.delete(Wrappers.<RoleDept>lambdaQuery().eq(RoleDept::getRoleId, role.getId()));
        // 是否是自定义数据权限
        if (DataScope.CUSTOM.getCode().equals(role.getDataScope()) && CollUtil.isNotEmpty(deptIds)) {
            // 添加数据
            List<RoleDept> roleDepts = new ArrayList<>();
            deptIds.forEach(s -> roleDepts.add(new RoleDept().setRoleId(role.getId()).setDeptId(s)));
            SqlHelper.executeBatch(RoleDept.class, this.log, roleDepts, DEFAULT_BATCH_SIZE, (sqlSession, entity) -> {
                String sqlStatement = SqlHelper.getSqlStatement(RoleDeptMapper.class, SqlMethod.INSERT_ONE);
                sqlSession.insert(sqlStatement, entity);
            });
        }
        return success;
    }

    /**
     * 删除角色
     *
     * @param id
     * @return
     */
    @CacheEvict(value = {"role:data-scope", "role:data-scope-flag", "user:role", "user:resource"}, allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteRole(String id) {
        boolean success = removeById(id);
        // 删除数据
        roleDeptMapper.delete(Wrappers.<RoleDept>lambdaQuery().eq(RoleDept::getRoleId, id));
        return success;
    }

    /**
     * 批量删除角色
     *
     * @param idList
     * @return
     */
    @CacheEvict(value = {"role:data-scope", "role:data-scope-flag", "user:role", "user:resource"}, allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatchRole(List<String> idList) {
        boolean success = removeByIds(idList);
        idList.forEach(s -> {
            // 删除数据
            roleDeptMapper.delete(Wrappers.<RoleDept>lambdaQuery().eq(RoleDept::getRoleId, s));
        });
        return success;
    }

    /**
     * 获取已授权的数据范围部门ids
     *
     * @param roleId
     * @return
     */
    public List<String> getDataScopeDeptIdsByRoleId(String roleId) {
        return roleDeptMapper.selectList(Wrappers.<RoleDept>lambdaQuery().eq(RoleDept::getRoleId, roleId)).stream().map(RoleDept::getDeptId).collect(Collectors.toList());
    }

    /**
     * 获取角色列表和是否有全部数据权限
     *
     * @param userId
     * @return
     */
    @Cacheable(value = "role:data-scope-flag", key = "#userId")
    public Map<String, Object> getAllDataScopeFlagAndDataByUserId(String userId) {
        // 设置数据范围类型
        final List<Role> roleList = userInfoMapper.getAuthRoleListByUserId(userId);
        final boolean anyMatch = roleList.stream().anyMatch(role -> DataScope.ALL.getCode().equals(role.getDataScope()));
        final Map<String, Object> map = new HashMap<>();
        // 角色列表
        map.put("roleList", roleList);
        // 是否有全部数据权限
        map.put("allDataScope", anyMatch);
        return map;
    }

    /**
     * 根据用户id和角色列表查询 获取所有的部门id
     *
     * @param userId
     * @param roleList
     * @return
     */
    @Cacheable(value = "role:data-scope", key = "#userId")
    @Transactional(readOnly = true)
    public Set<String> getDateScopeDeptIds(final String userId, final List<Role> roleList) {
        Set<String> deptIds = new HashSet<>();
        roleList.forEach(role -> {
            // 获取枚举值
            final DataScope dataScope = DataScope.getDataScopeByCode(role.getDataScope());
            if (dataScope != null) {
                switch (dataScope) {
                    case CUSTOM:
                        List<String> collect = roleDeptMapper.selectList(Wrappers.<RoleDept>lambdaQuery().eq(RoleDept::getRoleId, role.getId()))
                                .stream()
                                .map(RoleDept::getDeptId)
                                .collect(Collectors.toList());
                        deptIds.addAll(collect);
                        break;
                    case ME_DEPT:
                        // 获取用户信息
                        UserInfo userInfo = userInfoMapper.selectById(userId);
                        deptIds.add(userInfo.getDeptId());
                        break;
                    case ME_DEPT_CHILD:
                        UserInfo userInfo2 = userInfoMapper.selectById(userId);
                        if (StrUtil.isNotBlank(userInfo2.getDeptId())) {
                            List<String> newDeptList = new ArrayList<>();
                            newDeptList.add(userInfo2.getDeptId());
                            Dept dept = deptMapper.selectById(userInfo2.getDeptId());
                            // 查询第一层子级数据
                            List<Dept> deptList = deptMapper.selectList(Wrappers.<Dept>lambdaQuery().eq(Dept::getParentId, dept.getId()));
                            // 递归查询子级数据
                            getDeptChild(newDeptList, deptList);
                            deptIds.addAll(newDeptList);
                        }
                        break;
                }
            }
        });
        return deptIds;
    }

    private void getDeptChild(List<String> newDeptList, List<Dept> deptList) {
        if (CollUtil.isNotEmpty(deptList)) {
            deptList.forEach(dept -> {
                List<Dept> deptList1 = deptMapper.selectList(Wrappers.<Dept>lambdaQuery().eq(Dept::getParentId, dept.getId()));
                newDeptList.add(dept.getId());
                getDeptChild(newDeptList, deptList1);
            });
        }
    }
}
