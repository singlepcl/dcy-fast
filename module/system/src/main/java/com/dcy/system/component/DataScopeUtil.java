package com.dcy.system.component;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.dcy.common.context.BaseContextHandler;
import com.dcy.system.model.Role;
import com.dcy.system.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/3/29 16:02
 */
@Component
public class DataScopeUtil {

    @Autowired
    private RoleService roleService;

    /**
     * 数据过滤
     *
     * @param wrapper
     */
    public void dataScopeFilter(AbstractWrapper wrapper) {
        dataScopeFilter(wrapper, null);
    }

    /**
     * 数据过滤
     *
     * @param wrapper
     * @param deptAlias 主表别名
     */
    public void dataScopeFilter(AbstractWrapper wrapper, String deptAlias) {
        final String userId = BaseContextHandler.getUserID();
        final Map<String, Object> allDataScopeFlagAndData = roleService.getAllDataScopeFlagAndDataByUserId(userId);
        final Boolean allDataScope = MapUtil.getBool(allDataScopeFlagAndData, "allDataScope");
        if (!allDataScope) {
            // 有数据范围
            List<Role> roleList = MapUtil.get(allDataScopeFlagAndData, "roleList", new TypeReference<List<Role>>() {
            });
            Set<String> dataScopeList = roleService.getDateScopeDeptIds(userId, roleList);
            if (StrUtil.isNotBlank(deptAlias)) {
                wrapper.apply(StrUtil.format(" {}.dept_id in ({})", deptAlias, CollUtil.join(dataScopeList, ",", "\"", "\"")));
            } else {
                wrapper.apply(StrUtil.format(" dept_id in ({})", CollUtil.join(dataScopeList, ",", "\"", "\"")));
            }
        }
    }

}
